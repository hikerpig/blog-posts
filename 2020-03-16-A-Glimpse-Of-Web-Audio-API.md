---
layout: post
date: 2020-03-16
title: "初识 Web Audio API - 术语扫盲"
description: ""
category: 前端
tags:
- audio
- Avtech

use_toc: True
feature_image: https://i.loli.net/2020/03/17/nX3TarNg6IMwUYe.jpg
---

Web Audio 提供了一个基于图的音频处理构建体系，这个系统里的单元为各种音频节点（AudioNode），可模块化连接（modular routing）。这个体系被业界广泛认同多年，有很多成熟的应用和系统，例如 Apple [CoreAudio](https://developer.apple.com/documentation/coreaudio)，而这个设计理念再往上还能追溯到 1964 年的 [Moog](https://www.wikiwand.com/en/Moog_synthesizer) 合成器。

![A 1975 Moog Modular 55 system](https://i.loli.net/2020/03/17/KnWXYecoivIxS7r.jpg)

如果从 [Web Audio 规范文档](https://webaudio.github.io/web-audio-api) 初入这个领域，可能理解起来有点费力，毕竟文档给出了参数和实现细节，却并不解释这些命名的逻辑。

看看这张遍布着黑话的模块图：

![一个音频处理系统图](https://i.loli.net/2020/03/17/nX3TarNg6IMwUYe.jpg)

对一些基础概念的解释，MDN 的文档[《网页音频接口的基本概念》](https://developer.mozilla.org/zh-CN/docs/Web/API/Web_Audio_API/Basic_concepts_behind_Web_Audio_API)做得很好。

不过 MDN 的文档里对一些领域知识和行内术语的解释也不太详尽，以下是我碰到的几个初感迷惑，调查背景以后才理解了的概念。

## 效果节点

### GainNode

这是我第一个遇见的效果节点，需要改变音量的时候会用到，那么为什么不叫做 VolumeNode 呢？私以为，音量是一个比较主观和复杂的概念。而音频处理，基本上可以看做一个信号处理过程，无论是早年的模拟信号（电流、电压），还是现在的数字信号，关注点都在于信号本身。

Gain 一般翻译为增益，是电子学的术语，单独拎出来说没有意义，要细说的话有电压增益、电流增益、功率增益等。

### PannerNode

又一个看到名字不知道是做什么的东西， pan 的翻译是“平底锅”，和声音又有什么关系呢？ 在[维基百科里关于 Panning 的解释](https://www.wikiwand.com/en/Panning_(audio))里提到：

> 声音处理中的 Panning 借用了摄像中的“摇拍”概念。

![摄影中的摇拍](https://upload.wikimedia.org/wikipedia/commons/9/9c/Pan1.gif)

简单来说就是通过更改多个声道的相对强度，来营造出一种音源在移动的感觉。同时这个移动的感觉能够营造出空间感，所以在规范里这个技术与”空间化“（Spatialization）也放在一起讨论。

### BiquadFilterNode

双二阶滤波器（Biquad Filter）在电子学里很常用。按照百度百科的说法：

> 双二阶滤波器是传递函数的分子、分母都是二阶多项式的滤波器。

直观地从使用角度来理解，这个滤波器在两个不同的频段可以有不同的响应模型，所以通过合理配置参数，可以增强或者削减某些频段的强度，实现低通（lowpass）、高通（highpass）、带通（bandpass）等效果。详细响应曲线可以参考以下这个例子，来自[webaudioapi.com](https://webaudioapi.com/samples/frequency-response/)。

<iframe src="https://webaudioapi.com/samples/frequency-response/" height="600" class="iframe--resize-v"></iframe>

<!--以下是一个低通滤镜的响应曲线：

![low pass filter](https://i.loli.net/2020/03/17/jyV8N5axb1HUuA3.jpg)-->

### ConvolverNode

[规范中的 ConvolverNode 定义](https://webaudio.github.io/web-audio-api/#ConvolverNode)：

> The ConvolverNode interface is an AudioNode that performs a Linear Convolution on a given AudioBuffer, often used to achieve a reverb effect

对信号源和提供给卷积器的冲激响应（impulse response，以 AudioBuffer 的形式提供给卷积器，作为卷积核）做卷积，信号源和卷积核都是离散和线性的。一般可用于制造混响（reverb，或称回响）效果。

首先说到[冲激响应](https://www.wikiwand.com/zh-hans/%E5%86%B2%E6%BF%80%E5%93%8D%E5%BA%94)，指的是一个系统随着时间，对于单位输入信号（即一个脉冲）的响应。

以下的冲激响应来自 [webaudioapi room-effects 例子](https://webaudioapi.com/samples/room-effects/) 里的 telephone 效果。对于每一个脉冲来说，在发生的 0.0006 秒左右后开始有响应，大概 0.0025 秒之后趋于平静。在音效处理中，一般会将未经处理的声音（称作“干”声音，对应于题图中的 dry）与经过处理过的声音（称作“湿”声音，对应于 wet）按照一定比例混合，那么在电话的这个例子中，就能听到稍微有一点延后的效果。假如这个延后的时间内更长，就有大的房间里回音的效果。

![电话效果](https://i.loli.net/2020/03/18/CsUke2BOMqRPbQt.jpg)

blink 的源码中有一段关于 [convolver_reverb 的文档](https://github.com/chromium/chromium/blob/master/third_party/blink/renderer/modules/webaudio/docs/convolver_reverb.md)，叙述了实现卷积混响时减少计算量的一些技巧。

一般来说，冲激响应曲线由音效设计师录制或是制作，可以导出成 `.wav` 文件等。


