---
layout: post
date:   2015-02-02
title: "用 JS 写简单的 CSS Parser"
description: ""
category: 前端
tags:
- CSS
use_toc: False
---


## 用正则表达式提取规则

[JotForm团队的一篇文章][jotform_post]

```js
([\\s\\S]*?){([\\s\\S]*?)}
```

分解一下

```js

([\\s\\S]*?) => any string as selector

{ => then an opening bracket

([\\s\\S]*?) => any string as CSS rules

} => then a closing bracket
```

输入这个css文本:

```css
.someClass {
  margin : 20px;
}
```

得到结果是这样:

```js
[
  {
    "selector" : ".someClass",
    "rules" : [
      {
        "directive" : "margin",
        "value" : "20px"
      }
    ]
  }
]
```

好吧其实没那么容易, 例如Media Query就没考虑到吧

```js
((\\s*?@media[\\s\\S]*?){([\\s\\S]*?)}\\s*?))|(([\\s\\S]*?){([\\s\\S]*?)})
```

[jotform_post]: https://medium.com/jotform-form-builder/writing-a-css-parser-in-javascript-3ecaa1719a43
