---
layout: post
date:   2017-08-05
title: "Holy Awesome Spacemacs"
summary: 又名：多年 Vim 用户笑迎 Spacemacs
category: 工具
tags:
- emacs
- editor
- productivity
- 工具
use_toc: True
feature_qimage: image/emacs/screenshot.png
---

![](http://spacemacs.org/img/logo.svg)

很久以前看 SICP 的时候尝试在 windows 下使用 emacs 时受到了惊吓，那刀耕火种一般的界面啊。后来在 Ubuntu 终端中打开，诶？怎么关不掉？不掉？掉？（其实 Vim 也一样，小白终结者）

但是看完[如何评价 Emacs 的配置文件 Spacemacs？](https://www.zhihu.com/question/29692328)以及身边多人的安利后，心痒痒地尝试了下，从一个外行人眼里看来，简直是 Emacs 界的一股清流，史诗级的配置工程。


安装
==

鉴于有 Every thing in Emacs 的传统，选择了可以不光在终端里使用，便安装了带有 Mac GUI 系统支持的 Emacs：

```
brew install emacs --with-cocoa
```


Spacemacs
-- 

完全没有历史的 `.emacs.d` 包袱，一键安装

```
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
```

开始使用
==

还想说打开速度果然比 Vim 慢了不少，但是慢慢使用着发现，考虑到这些巨量的功能以及超棒的体验来说，等得太值了。重点是，**开箱即用**。

选择了 Setup Guile 中提示了相对较重的 `helm` layer，以及开了其他七八个 layer。不考虑首次安装插件的时间，之后平均启动时间在15秒内。

然后开始领教 Emacs 的能量：

org-mode
==

江湖相传能用于[组织你的意念](http://docs.huihoo.com/open-source/guide/ch32.html)的“文档/知识/日程/<del>人生</del>管理工具”，神级插件。

在这之前，使用 [Vimwiki](http://vimwiki.github.io) 管理个人知识，在 Vim 里的表现也是很棒的，但是折腾起来总有几点不甚满意，但在 org-mode 下找到了完美的解决方案。

文档转换
--

在 Markdown 大行其道的今天，还需要单独去记忆 Vimwiki 自己的语法，有时候转不过弯来。还有，有时候一些收集性质的笔记，需要从 html 转换过来，Vimwiki 在这方面相关工具比较匮乏。

`org-mode` 虽说也是自成一派的语法，但是相关工具链非常完善，与各种文档格式互转毫无压力，例如 [vimwiki2org](https://github.com/fasheng/vimwiki2org)。

当需要集成现有工具到编辑器里时，一边是定义清晰、文档完善、继承了 Lisp 系美感的 Emacs Lisp ，和时常让人呼唤神兽的 Vim Script。

代码块支持
--

在 Vimwiki 里插入和查阅代码块的体验，纯粹而贫乏。

同样一个 python 代码块，在 Vimwiki 里：

{% qimage image/emacs/vimwiki-code.png %}

而在 org-mode 当中一切都很行云流水

{% qimage image/emacs/org-mode-code.png %}

其他优点
--

包括但不限于：

- 所见即所得，默认情况下 live preview，不会生成多余文件
- 强大的链接系统，直接跳转至多种形式的内外链接
- 强大的[日程规划功能](http://orgmode.org/manual/Agenda-Views.html)
- 强大的[表格编辑功能](http://www.cnblogs.com/holbrook/archive/2012/04/12/2444992.html)

### 一些收集

- [Organize Your Life In Plain Text!](http://doc.norang.ca/org-mode.htm)

                                        
helm
==

[A Package in a league of its own: Helm](http://tuhdo.github.io/helm-intro.html)


交互形式让我想起了 Vim 下的 `ctrlp` 和 `unite`，但是能做的事情更多，而且扩展性更好。

除了能找到任何文件外，还能找个任何一个命令，再也不用记那么多 `M-x` 命令了，再也不用担心小拇指受伤了。

![From linux.cn](https://dn-linuxcn.qbox.me/data/attachment/album/201601/15/103214s4z77zo4pimefccm.jpg)


scala-mode 以及 ensime
==

在做 Coursera 上 Scala 课程作业的时候试了一下。[Ensime](http://ensime.org/) 的 Emacs 插件完成度[非常的高](http://ensime.org/editors/)。

artist-mode
==

无比炫酷的 ACSII Art 工具，写注释和文档有时一图胜千言。配合 org-mode，感觉自己从未如此热爱写文档。

看[emacser 上的一篇介绍](http://emacser.com/artist-mode.htm)。

Vim 上有个类似的插件 [Drawit.vim](http://www.vim.org/scripts/script.php?script_id=40)，不知二者谁先谁后，都挺好用的。

imenu-list
========

在右边新建一个 buffer 显示 imenu 的结果，接近于 Vim 的 Tagbar 使用体验。

![github imenu-list](https://github.com/bmag/imenu-list/raw/master/images/imenu-list-dark.png)


可以使用，但不准备替换现有主力编辑器（Atom/Vim）的
==

coffee-mode
--

[coffee-mode](https://github.com/defunkt/coffee-mode) 支持 coffeescript 文件的语法高亮、编译和 watch 等功能，配合 etags 使用还不错。

与 evil 一起使用的时候需要一些额外配置，否则用 `o` 换行的时候发现缩进不对。

官方 README 给出了解决方案：

```elisp
(custom-set-variables
 '(coffee-indent-like-python-mode t))
```


放弃折腾的部分
==

Javascript 和 Eslint
--

作为一个前端，还是有点小遗憾的。

没有找到开箱即用的插件，`flycheck` 似乎很是强大，但是要支持 eslint 还有改许多配置。当然了，前端开发还是看 VS Code 和 Atom 吧。
