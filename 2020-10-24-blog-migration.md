---
layout: post
date: 2020-10-24
title: "博客改写迁移记"
description: "记录博客从 Hexo 迁移至 Gatsby 的一些考量和操作"
category: "杂记"
use_toc: True
tags:
- 折腾
- Gatsby
- 博客
---

最近看到 [Digital Garden](https://mathieudutour.github.io/gatsby-digital-garden/) 这个 Gatsby 主题，觉得 Gatsby 这种轻易转换不同数据源（一般多通过 remark 支持 markdown 文件， 作者自己订制支持了 Roam Research）的能力很不错。

决定迁移一下自己旧的 Hexo 博客。

## 从 Hexo 迁移至 Gatsby

Gatsby 的许多理念在 JS site-generator 领域是先进的，例如关于 source 和 transformer 的优秀概念实现、基于 React 的前后端同构及SSR、GraphQL（见仁见智，概念爆炸提高了入门门槛，但熟悉了之后觉得灵活度还是挺高的，尤其是 dev 模式下的调试界面很友好）等。有一个非常大的槽点是各种依赖实在是太多了，尤其是当你使用的是它的某个功能丰富的 starter，会陷入 `node_modules` 噩梦中。同时，构建时的速度在横向比较时也不是很拿得出手。

（虽然还是怀念 Hexo 的简单快速，以及许多对于技术博客的场景已经很好的主题）

尝试的一些其他方案有：

- [VuePress](https://vuepress.vuejs.org/)，一开始的选择。不过用了一圈才感觉，从一开始就朝着快速的文档站生成器做的 VuePress 在往其他数据源方向改造时，有点费劲。且流行的几个博客主题实现方式不尽相同，与官方的 blog 插件实现也不太相同，许多事情还需要挖源码才知道。
- [Gridsome](https://gridsome.org/)，可以简单概括为 Vue.js 生态下的类 Gatsby 系统，目前还没到 1.0，而且生态还在发展中，相应插件不算多。尽管比起 React 我更喜欢 Vue，也没有选择它。
- [Hugo](https://gohugo.io/)，目前速度最快的静态站点生成器，有很多优秀的博客模板。不过因为是 Go 写的，折腾起来更费劲了，也不太考虑。

## 构建和部署

开始写博客的时候使用的是 Jekyll 和 Hexo，前者是 Ruby 写的，生态与 JavaScript 有点不对付，自己订制一些功能稍嫌麻烦。而 Hexo 在没有 Github Actions 的时候构建和部署没有太多方便的选择，所以我用的 hexo deployer 在本地构建之后将产物推送至 github pages。

这样的方式需要我在自己的电脑或是 VPS 上手动构建，有时候换了电脑且访问 Vultr 不太顺畅的时候就什么都做不了。

穷则思变，想起来目前支持 JAMStack 的云服务商们很慷慨，是时候好好薅一下羊毛了。

Netlify 和 Vercel 的速度和稳定性都很优秀，相比之下我在别的仓库中碰见过 github actions 和 Azure 的几次神奇报错。个人博客用量不大不会超过免费用量限制，也不需要在 Github 上协作，因此最后选择了使用 Netlify 构建。

### 项目结构和构建过程

博客分为 2 个 git 仓库：

- blog-posts，存放纯粹的文章，带有 FrontMatter 的 Markdown 文件
- blog-site，存放 Gatsby 相关的建站代码

专门写了一个用于 `npm run netlify-build` 的脚本。

```mermaid
graph TD
    A[Clone blog-site 仓库]
    B[拉取 blog-posts 仓库 zip 并解压]
    C[gatsby build]
    D[完成]
    S[开始] --> A
    A --> B
    B --> C
    C --> D
```

在 Netlify 设置构建和部署的时机:

- 每一次 `blog-site` master 分支的 push
- 每一次 `blog-posts` 的 push (通过 Web Hook 来调用 blog-site 的构建)

多说一些，Vercel 也是非常优秀的服务提供商，不过因为我图简单使用了 `curl` 来拉取 `blog-posts-master.zip`，在其默认 Node.js 项目运行环境里下没有这个程序（不过似乎 [环境可以自己定制](https://vercel.com/docs/runtimes?query=runtime#advanced-usage/developing-your-own-runtime)），便没有再试下去。

## 域名迁移

这次使用自己的域名 `www.hikerpig.cn` 解析到 Netlify 的 IP 上，前面还加了层 Cloudflare 的 CDN。

这样就面临一个问题，之前 Github Pages 的 `https://hikeprig.github.io` 域名存在了几年，有一些搜索引擎的收录，不再更新 pages 之后，如何用尽量少的工作使得访问旧域名时能够被引导到新域名呢？

### HTML Meta Refresh Redirect

鉴于我对 github.io 域名以及服务没有任何更改的权限，因此只好在生成的静态产物上做一些手脚。

例如原先 url 为 `https://hikerpig.github.io/post-1/` 的页面，希望重定位到 `https://www.hikerpig.cn/post-1/`。

在 `<head>` 标签内加入：

```html
<meta http-equiv="refresh" content="0; url='https://www.hikerpig.cn/post-1/" />
```

浏览器解析完成之后会自动进行重定向。

### Canonical URL

纯粹出于对 SEO 的考量， 对旧的页面还添加了一行 HTML，告诉搜索引擎“权威”的数据源 URL 在新的网站里。

```html
<link rel=“canonical” href="https://www.hikerpig.cn/post-1/" />
```

## 参考

- [HTML redirect - HTML meta refresh redirect](https://www.rapidtables.com/web/dev/html-redirect.html)
- [Canonical Tags: A Simple Guide for Beginners](https://ahrefs.com/blog/canonical-tags/)
