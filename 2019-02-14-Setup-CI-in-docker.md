---
layout: post
date:   2019-02-15
title: "在 docker 环境中从 gitlab 拉取源码"
description: "在 docker 环境中从 gitlab 拉取源码"
category: 运维
tags:   
- docker
- 前端
- git
---

前注：在 docker 镜像中拉取 git 仓库似乎是一种 anti-pattern，本文只是记录中间的踩坑经历。

托管在公司 gitlab 上的一个前端项目，最近在尝试 docker 化。

# 准备阶段

## Gitlab 上的一些问题

如果项目不是公开的，从 docker 环境中拉取代码会有些许权限问题，可一一解决。

### clone with deploy token

到 Gitlab 中对应项目的 settings/repository 页面，生成 Deploy Tokens, 便可以使用 https clone 项目，不用输入用户密码，也不必使用设置相对比较麻烦的 ssh deploy key 方式。

克隆项目的命令为

```bash
git clone https://<name>:<token>@gitlab.com/<USER>/<REPO>.git
```

此处的 `name` 是 token name, 不是用户名。只对此项目生效，避免使用权限较高的用户 token。

###  git-lfs 拉取文件出错

即便使用了 deploy token 可以克隆项目文件，但其中使用 git-lfs 追踪的一些文件会失败：

```txt
Error downloading object: some/filename/blah: Smudge error: https://<name>:<token>@gitlab.com/some/repo.git/info/lfs/objects/batch
```

应该是权限问题，可以越过 git-lfs 的 smudge 步骤来规避此问题。

```bash
GIT_LFS_SKIP_SMUDGE=1 git clone <SERVER-REPOSITORY>
```

参考:  https://stackoverflow.com/questions/42019529/how-to-clone-pull-a-git-repository-ignoring-lfs

# 开始

## Dockerfile

```
FROM node:8.15.0-slim

# install git-lfs
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh |  bash
RUN apt-get install -y git-lfs

WORKDIR /app

RUN GIT_LFS_SKIP_SMUDGE=1 git clone https://<name>:<token>@gitlab.com/<USER>/<REPO>.git myrepo

WORKDIR /app/myrepo

RUN yarn
```

## 构建镜像

在 Dockerfile 所在目录下:

```bash
docker build -t my-app .
```

等待几分钟，构建完成便可使用 `docker inspect my-app` 命令看看新添加的镜像。

若 package.json 依赖较多，且可以接受在 docker host 和 vm 之间共享文件，可以去掉 `RUN yarn` 这一步，然后使用 [docker volume](https://docs.docker.com/storage/volumes/) 共享宿主环境中已经提前安装好的 node_modules 文件夹。

## 启动容器

启动一个带 tty 的 interactive 容器并运行 bash，可以在容器里转一转，看看有什么东西:

```bash
docker run -v /path/to/current/node_modules:/app/myrepo/node_modules -it my-app  --name app-tty /bin/bash
```

或是直接跑测试:

```bash
docker run -v /path/to/current/node_modules:/app/myrepo/node_modules -it my-app  --name test-container /bin/bash -c 'cd /app/myrepo && npm run test'
```