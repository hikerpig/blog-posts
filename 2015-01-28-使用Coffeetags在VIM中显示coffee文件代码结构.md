---
layout: post
date:   2015-01-28
title: "使用Coffeetags在VIM中显示coffee文件代码结构"
description: ""
category: 
tags:
- Vim
- Coffeescript

no_summary: True
use_toc: False

---

VIM的插件[Tagbar][tagbar]可以在侧边栏显示当前代码结构，不过Tagbar使用的ctags不支持coffeescript。

不过我们可以使用ruby版本的[CoffeeTags][coffeetags]生成tags。

首先:

```bash
$ [sudo] gem install CoffeeTags
```

然后只要在~/.vimrc里加上下面几行配置。(参考[CoffeeTags项目自带的vim插件][coffeetags_vim])

```vim
if executable('coffeetags')
  let g:tagbar_type_coffee = {
    \ 'ctagsbin' : 'coffeetags',
    \ 'ctagsargs' : '',
    \ 'kinds' : [
      \ 'c:classes',
      \ 'm:methods',
      \ 'a:attributes',
      \ 'f:functions',
      \ 'v:variables',
      \ 'p:prototypes',
      \ 'o:object',
      \ 'b:blocks'
    \ ],
    \ 'sro' : ".",
    \ 'kind2scope' : {
      \ 'f' : 'function',
      \ 'o' : 'object',
    \ }
  \ }
endif
```

就可以浏览清晰的coffee结构啦。

{% qimage image/coffeetags_tagbar.jpg %}

[tagbar]: https://github.com/majutsushi/tagbar
[tagbarwiki]: https://github.com/majutsushi/tagbar/wiki
[coffeetags]: https://github.com/lukaszkorecki/CoffeeTags
[coffeetags_vim]: https://github.com/lukaszkorecki/CoffeeTags

[coffeectags_gist]: https://gist.github.com/bjornharrtell/2901844
[coffeectags_gist_2]: https://gist.github.com/jesstelford/6134172
[coffeectags_gist_3]: https://gist.github.com/edubkendo/2901380
