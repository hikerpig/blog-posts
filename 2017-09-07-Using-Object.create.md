---
layout: post
date:   2017-09-07
title: "使用 Object.create(null) 创建空对象"
description: ""
category: 前端
tags:
- Javascript
---

使用对象字面量形式时，隐式地创建了一个以 `Object.prototype` 作为原型的对象。

以下两种方式等同。

```js
var o1 = {}

var o2 = Object.create(Object.prototype)
```

## 更省事的方法

```js
var o3 = Object.create(null)
```

创建一个不继承任何原型的对象。

如果需要创建一个简单的 map 性质的对象，使用此种方式，在对 `o3` 进行遍历的时候，就可以躲开充满恶意的世界在 `Object.prototype` 上添加的方法或属性。

```js
for (const k in o1) {
  if (o1.hasOwnProperty(k)) {
  }
}

// 遍历时不需要判断 hasOwnProperty
for (const k in o3) {
}
```