---
layout: post
date:   2018-07-31
title: "Goodbye My Santa Monica Dream 🎵"
description: ""
category: 杂记
tags:
- 吉他
- 音乐
---

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="//music.163.com/outchain/player?type=2&id=30496500&auto=0&height=66"></iframe>

从《Life is strange》的原声带中发现的瑰宝，来自澳大利亚的兄妹组合 [Angus & Julia Stone](https://music.douban.com/musician/100215/) 的一首略显忧伤的曲子，他俩前几年的曲风，小清新中带着些许迷幻。妹妹声音空灵慵懒，像加州的海风，棕榈叶间斑驳的阳光，和歌词特别配。

原曲使用 Open D 调弦，变调夹 3 品，感谢 [Goliath Guitar](http://www.goliathguitartutorials.com) 在 [Youtube 上的教程](https://www.youtube.com/watch?v=sOJMMpdWScM)，讲解详细，也给出了还原度几乎 100% 的伴奏谱子，而且非常好弹。pdf 谱例可以[去这里下载](http://www.goliathguitartutorials.com/santa-monica-dream.html)。


{% qimage image/tabs/santa_monica_dream_-_angus_and_julia.jpg %}

