---
layout: post
date:   2015-02-05
title: "用Tagbar在VIM中查看scss文件代码结构"
description: ""
category: 前端
tags: ['Vim', 'CSS']
use_toc: False

no_summary: true

---

在`~/.ctags`里加上这几行定义下scss文件的规则:

```text
--langdef=scss
--langmap=scss:.scss
--regex-scss=/^[ \t]*([^\t {}][^{}]{1,100})(\t| )*\{/| \1/d,definition/
--regex-scss=/^[@]mixin ([^ ()]+).*/\1/m,mixin/
--regex-scss=/^[@]function ([^ ()]+).*/\1/f,function/}
--regex-scss=/^\$([A-Za-z0-9._-]+)\s?:.*/\1/v,variable/}
```

在`~/.vimrc`里加上这几行(前提是VIM有安装Tagbar插件哦)

```vim
let g:tagbar_type_scss = {
  \ 'ctagsbin'     : 'ctags',
  \ 'ctagstype'     : 'scss',
  \ 'kinds'     : [
      \ 'd:definition',
      \ 'f:functions',
      \ 'm:mixins',
      \ 'v:variables',
  \ ],
\ }
```

写正则真是越来越好玩了。

[so_1]: http://stackoverflow.com/questions/10724140/how-to-make-ctags-scss-play-nice/10725882#10725882
