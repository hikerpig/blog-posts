---
layout: post
date:   2017-08-22
title: "给开发减压的 gulp.watch 配置"
description: ""
category: 
tags: []
---

Gulp 的文件监听使用了 [gaze](https://github.com/shama/gaze)，如果只看了 Gulp 的 API 文档可能会漏掉一些参数。

[Gulp 文档](https://github.com/gulpjs/gulp/blob/master/docs/API.md) 中提到:

`gulp.watch(glob[, opts], tasks)`

```
opts

Type: Object

Options, that are passed to gaze.
```

options 包括:

- `interval` {integer} Interval to pass to fs.watchFile
- `debounceDelay` {integer} Delay for events called in succession for the same file/event in milliseconds
- `mode` {string} Force the watch mode. Either 'auto' (default), 'watch' (force native events), or 'poll' (force stat polling).
- `cwd` {string} The current working directory to base file patterns from. Default is process.cwd()

gaze 的设置参数中，感觉最有用却被遗漏的就是 `interval` 了，好多人提到 [gulp.watch CPU 占用太高](https://github.com/gulpjs/gulp/issues/634)，因为 gaze 默认 100ms 检查监听的文件状态。

如果监听文件太多导致 CPU 占用居高不下的话，可以考虑加大 `interval`:

```js
gulp.watch(blob, {interval: 500}, tasks)
```

笔记本就不会呼呼乱叫了。
