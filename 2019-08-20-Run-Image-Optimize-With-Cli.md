---
layout: post
date: 2019-08-20
title: "命令行下的图片压缩工具"
summary: "推荐几个命令行下的图片压缩工具 ImageOptim-CLI/imagemin-cli"
category: 工具
tags:
- 图片
- 工具
use_toc: True
---

# imagemin-cli

[imagemin/imagemin-cli](https://github.com/imagemin/imagemin-cli) 是一个用 node.js 写的工具，调用的都是纯 CLI 的开源程序，所以平台兼容性好很多。其依赖的 [imagemin](https://github.com/imagemin/imagemin) 项目支持 `imagemin-*` 形式的 npm package 插件扩展，cli 默认的插件有这 4 个。

```
	'gifsicle'
	'jpegtran'
	'optipng'
	'svgo'
```

使用的时候会检测当前系统变量里有没有对应的可执行文件，没有的话会自动下载安装，真正做到了开箱即用。imagemin 在前端自动化工作流中有诸多支持，例如 [imagemin-webpack-plugin](https://github.com/Klathmon/imagemin-webpack-plugin)，[gulp-imagemin](https://github.com/sindresorhus/gulp-imagemin)。

官方维护的插件列表和项目构建支持可见 [github 下的 imagemin 组织](https://github.com/imagemin) 。

## 使用示例

`--help` 里给的例子:

```
  Examples
    $ imagemin images/* --out-dir=build
    $ imagemin foo.png > foo-optimized.png
    $ cat foo.png | imagemin > foo-optimized.png
    $ imagemin --plugin=pngquant foo.png > foo-optimized.png
```



# ImageOptim-CLI

仅 Mac 可用，[ImageOptim-CLI](https://github.com/JamieMason/ImageOptim-CLI) 包装了几个 Mac 下 GUI/CLI 图片压缩工具的调用，支持调用的 App 有：

    Supported Apps:
    
    ImageAlpha: https://pngmini.com
    ImageOptim: https://imageoptim.com
    JPEGmini Lite: https://itunes.apple.com/us/app/jpegmini-lite/id525742250
    JPEGmini Pro: https://itunes.apple.com/us/app/jpegmini-pro/id887163276
    JPEGmini: https://itunes.apple.com/us/app/jpegmini/id498944723

常见的 JPEG/PNG/GIF/SVG 等格式都可以压缩。

调用方式很暴力，直接硬编码了应用的路径。

```js
export const PNGQUANT_BIN_PATH = '/Applications/ImageAlpha.app/Contents/MacOS/pngquant';
export const IMAGEOPTIM_BIN_PATH = '/Applications/ImageOptim.app/Contents/MacOS/ImageOptim';
```

所以先要提前安装好相应的 App。

## 安装

```bash
npm install -g imageoptim-cli
```

默认使用的是 [ImageOptim](https://imageoptim.com/mac)，一个开源的 GUI 图片压缩工具，集成了针对各种格式图片的优秀开源压缩工具。

> [ImageOptim](https://imageoptim.com/) is a GUI for lossless image optimization tools: Zopfli, PNGOUT, [OxiPNG](https://crates.rs/crates/oxipng), AdvPNG, PNGCrush, [JPEGOptim](https://github.com/tjko/jpegoptim), Jpegtran, [Guetzli](https://github.com/google/guetzli), [Gifsicle](https://kornel.ski/lossygif), [SVGO](https://github.com/svg/svgo), [svgcleaner](https://github.com/RazrFalcon/svgcleaner) and [MozJPEG](https://github.com/mozilla/mozjpeg).

```bash
brew cask install imageoptim
```

## 使用

处理 JPEG 格式还是挺快的。

```bash
$ imageoptim '**/*.jpg' '**/*.jpeg'
i Running ImageOptim...
✓ coffeetags_tagbar.jpg was: 845kB now: 786kB saving: 58.4kB (6.91%)
✓ compare-shadow-blur.jpg was: 22.4kB now: 22.4kB saving: 0B (0.00%)
✓ stack-or-gtfo.jpg was: 98.1kB now: 68.1kB saving: 30kB (30.59%)
✓ truck.jpg was: 449kB now: 401kB saving: 47.7kB (10.63%)
✓ fonts/kosugi-maru-1.jpg was: 22.9kB now: 14.4kB saving: 8.54kB (37.29%)
✓ fonts/noto-sans-jp-1.jpg was: 24.9kB now: 15.8kB saving: 9.17kB (36.79%)
✓ tabs/santa_monica_dream_-_angus_and_julia.jpg was: 263kB now: 189kB saving: 73.4kB (27.94%)
✓ CSS_EX1.jpeg was: 538kB now: 495kB saving: 43.6kB (8.10%)
✓ TOTAL was: 2.26MB now: 1.99MB saving: 271kB (11.97%)
✓ Finished
```

处理 PNG 时有点慢，可能是因为使用底层的 [pngquant](https://pngquant.org/) 时的参数问题，计算量有点大。

```bash
$ time imageoptim '**/*.png'
i Running ImageOptim...
✓ org-mode-code.png was: 36.6kB now: 15.8kB saving: 20.8kB (56.78%)
✓ screenshot.png was: 109kB now: 42kB saving: 67.2kB (61.55%)
✓ vimwiki-code.png was: 34kB now: 14.3kB saving: 19.7kB (57.90%)
✓ TOTAL was: 180kB now: 72.1kB saving: 108kB (59.89%)
✓ Finished
imageoptim '**/*.png'  146.20s user 1.37s system 290% cpu 50.865 total
```

如已经安装了 [ImageAlpha](https://pngmini.com) ，加上参数 `--imagealpha`，先调用 ImageAlpha，处理 PNG 的效率和速度都有很大提升。

```bash
$ time imageoptim --imagealpha '**/*.png'
i Running ImageAlpha...
i Running ImageOptim...
✓ org-mode-code.png was: 36.6kB now: 5.64kB saving: 31kB (84.60%)
✓ screenshot.png was: 109kB now: 15.8kB saving: 93.4kB (85.56%)
✓ vimwiki-code.png was: 34kB now: 4.72kB saving: 29.3kB (86.11%)
✓ TOTAL was: 180kB now: 26.1kB saving: 154kB (85.47%)
✓ Finished
imageoptim --imagealpha '**/*.png'  55.98s user 0.75s system 298% cpu 18.985 total
```

CLI 提供了 `--quality` 和 `--speed` 两个细化的调整参数，仔细调整可能有更合适的速度/压缩率组合。

对于不同工具对不同格式和特性的压缩效果，作者给出了[一份评测数据](http://jamiemason.github.io/ImageOptim-CLI/)。

