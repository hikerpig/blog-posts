---
layout: post
date:  2019-03-14
title: "Handy Video Analyze Tool"
description: 分享一些学习 avcodec 技术时好用的分析工具
category: 工具
tags:
- video
keywords:
- 视频技术
- 视频工具
- mp4
use_toc: True
feature_qimage: image/iso-viewer.png
---

最近在写一些跟视频解码相关的简单程序，学习 avcodec 技术时有一些强大的分析工具，分享一下。

# 码流与封装容器相关

## ISO Viewer

[isoviewer](https://github.com/sannies/isoviewer) 是 Mac 下一个检查 mp4 文件构成的 GUI 程序，能直观地看到各个 box 的数据。

{% qimage image/iso-viewer.png %}

## bento4

[bento4](https://www.bento4.com/) 是一套专用于 mp4 格式文件分析和编辑的 SDK，支持的功能非常多。


| 工具 | 用途 |
|  ---------------- |-------------------------------------------------------------------------------------------------------------------------- |
|  mp4info         | displays high level info about an MP4 file, including all tracks and codec details
|  mp4dump         | displays the entire atom/box structure of an MP4 file
|  mp4edit         | add/insert/remove/replace atom/box items of an MP4 file
|  mp4extract      | extracts an atom/box from an MP4 file
|  mp4encrypt      | encrypts an MP4 file (multiple encryption schemes are supported)
|  mp4decrypt      | decrypts an MP4 file (multiple encryption schemes are supported)
|  mp4dcfpackager  | encrypts a media file into an OMA DCF file
|  mp4compact      | converts 'stsz' tables into 'stz2' tables to create more compact MP4 files
|  mp4fragment     | creates a fragmented MP4 file from a non-fragmented one.
|  mp4split        | splits a fragmented MP4 file into discrete files
|  mp4tag          | show/edit MP4 metadata (iTunes-style and others)
|  mp4mux          | multiplexes one or more elementary streams (H264/AVC, H265/HEVC, AAC) into an MP4 file
|  mp42aac         | extract a raw AAC elementary stream from an MP4 file
|  mp42avc         | extract a raw AVC/H.264 elementary stream from an MP4 file
|  mp42hls         | converts an MP4 file to an HLS (HTTP Live Streaming) presentation, including the generation of the segments and .m3u8 playlist as well as AES-128 and SAMPLE-AES (for Fairplay) encryption. This can be used as a replacement for Apple's mediafilesegmenter tool.
|  mp42ts          | converts an MP4 file to an MPEG2-TS file.
|  mp4dash         | creates an MPEG DASH output from one or more MP4 files, including encryption. This is a full-featured MPEG DASH packager.
|  mp4dashclone    | creates a local clone of a remote or local MPEG DASH presentation, optionally encrypting the segments as they are cloned.
|  mp4hls          | creates a multi-bitrate HLS master playlist from one or more MP4 files, including support for encryption and I-frame-only playlists. This tool uses the 'mp42hls' low level tool internally, so all the options supported by that low level tool are also available. This can be used as a replacement for Apple's variantplaylistcreator tool.

## GPAC

[GPAC](https://gpac.wp.imt.fr/) 是一个为科研和学术领域开发的多媒体的框架，包括几个视频分析和码流编辑的 CLI 工具。官网主要 feature 如下:

> GPAC features encoders and multiplexers, publishing and content distribution tools for MP4 and 3GPP or 3GPP2 files and many tools for scene descriptions (BIFS/VRML/X3D converters, SWF/BIFS, SVG/BIFS, etc…). MP4Box provides all these tools in a single command-line application. An exhaustive list of packaging features is available here but a good summary of what MP4Box can do for you is the following:
>
>- MP4/3GP Conversion from MP3, AVI, MEPG-2 TS, MPEG-PS, AAC, H263, H264, AMR, and many others,
>- 3GPP DIMS Packaging from SVG files,
>- File layout: fragmentation or interleaving, and cleaning,
>- File hinting for RTP/RTSP and QTSS/DSS servers (MPEG-4 / ISMA / 3GP / 3GP2 files),
>- File splitting by size or time, extraction from file and file concatenation,
>- XML information dumping for MP4 and RTP hint tracks,
>- Media Track extractions,
>- ISMA E&A encryption and decryption,
>- 3GPP timed text tools (SUB/SRT/TTXT/TeXML), VobSub import/export,
>- BIFS codec and scene conversion between MP4, BT and XMT-A,
>- LASeR codec and scene conversion between MP4, SAF, SVG and XSR (XML LASeR),
>- XML scene statistics for BIFS scene (BT, XMT-A and MP4),
>- Conversion to and from BT, XMT-A, WRL, X3D and X3DV with support for gzip.
>- Preparing MP4,3GP and MPEG-2 TS files for DASH streaming

主要使用的可能是 `MP4Box` 工具，功能很多，几个例子:

```bash
MP4Box -add file.av1 file_av1.mp4 # 将 av1 码流封装写入 mp4 文件
```


## ffprobe

大名鼎鼎的 ffmpeg 项目一员，一身[技能和 API 参数](https://ffmpeg.org/ffprobe.html)。

其中一个用法是 dump 出所有的 frame:

```
ffprobe -show_frames some.mp4 > probe-mp4.txt
```

```
[FRAME]
media_type=video
stream_index=0
key_frame=1
pkt_pts=0
pkt_pts_time=0.000000
pkt_dts=0
pkt_dts_time=0.000000
best_effort_timestamp=0
best_effort_timestamp_time=0.000000
pkt_duration=512
pkt_duration_time=0.040000
pkt_pos=48
pkt_size=27979
width=320
height=564
pix_fmt=yuv420p
sample_aspect_ratio=N/A
pict_type=I
coded_picture_number=0
display_picture_number=0
interlaced_frame=0
top_field_first=0
repeat_pict=0
color_range=unknown
color_space=unknown
color_primaries=unknown
color_transfer=unknown
chroma_location=left
[/FRAME]
[FRAME]
media_type=audio
stream_index=1
key_frame=1
pkt_pts=0
pkt_pts_time=0.000000
pkt_dts=0
pkt_dts_time=0.000000
best_effort_timestamp=0
best_effort_timestamp_time=0.000000
pkt_duration=1024
pkt_duration_time=0.021333
pkt_pos=28243
pkt_size=6
sample_fmt=fltp
nb_samples=1024
channels=2
channel_layout=stereo
[/FRAME]
```

# 二进制显示/编辑

## Hex Fiend

[Hex Fiend](https://github.com/ridiculousfish/HexFiend), Mac 下的 hex 编辑 GUI 工具，支持按文本或hex搜索/编辑/diff，功能强大。

![HexFiend](https://raw.githubusercontent.com/ridiculousfish/HexFiend/master/docs/screenshot.png)

## hexyl

[hexyl](https://github.com/sharkdp/hexyl) ，一个轻量 CLI 查看二进制数据的工具，用 rust 编写。优点是快速，好看。

![hexyl 效果图](https://camo.githubusercontent.com/1f71ee7031e1962b23f21c8cc89cb837e1201238/68747470733a2f2f692e696d6775722e636f6d2f4d574f3975534c2e706e67)