---
layout: post
date: 2020-06-05
title: "工作和个人项目使用不同 gitconfig"
description: ""
category: 工具
tags:
- tip
- git

---

首先在  `~/.gitconfig` 中设定如下，注意 gitdir 后面的 `~/work/` 末尾的 `/` 不能少。

```
[includeIf "gitdir:~/work/"]
  path = ~/.gitconfig-company

[includeIf "gitdir:~/projects/"]
  path = ~/.gitconfig-personal
```

然后这两个文件就可以单独设置一些项目了。

例如 ~/.gitconfig-personal 文件内容，可以使用与工作时不同的用户设置：

```
[user]
  name = 'hikerpig'
  email = 'hikerpigwinnie@gmail.com'
```

最后可在某项目路径下使用 `git config --list --show-origin` 验证设置是否正确。

