---
layout: post
title: "git hook中的$GIT_DIR"
date:   2014-3-23 10:09:12
description: ""
category: 工具
tags:
- Git
no_summary: True
---

一直使用githook post-update来部署我的jekyll博客，每次push到远程git仓库的时候，在vps上的生产目录会自动更新和重新生成页面和静态资源， post-update文件是这样的：

```bash
#!/bin/usr/zsh

branchName=$(git rev-parse --symbolic --abbrev-ref $1)
HOME=/home/******
BLOG_DIR=$HOME/*******

cd $BLOG_DIR
git pull origin $branchName

if [ $branchName = "master" ]; then
  [ -s $HOME/.nvm/nvm.sh ] && . $HOME/.nvm/nvm.sh # This loads NVM
  export PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
  nvm use 113
  $HOME/.rvm/bin/rvm use ruby-2.0.0
  jekyll build
  make front
fi

exec git update-server-info
```

但是执行的时候会出现一个奇怪的问题：

```bash
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 281 bytes | 0 bytes/s, done.
Total 3 (delta 2), reused 0 (delta 0)
remote: fatal: Not a git repository: '.'
...
```

最后的这一句表示出错了，而且git pull也没有正确进行。

有人在[这个问题][SO_article]中谈到解决方法，改进的部分在这里：

```bash
cd $BLOG_DIR || exit
unset GIT_DIR
git pull origin $branchName
```

之后果然就可以正确地pull代码了。

[SO_article]: (http://stackoverflow.com/questions/4043609/getting-fatal-not-a-git-repository-when-using-post-update-hook-to-execut)
[enviroment_variable]:(https://www.kernel.org/pub/software/scm/git/docs/#_environment_variables)
