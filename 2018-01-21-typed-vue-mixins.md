---
layout: post
date:   2018-01-21
title: 在 Typescript 中使用可被复用的 Vue Mixin
description: ''
category: 前端
use\_toc: true
tags:
  - Typescript
  - Vue
style: ocean
---

转到用 Typescript 写 Vue 应用以后，经过一轮工具链和依赖的洗礼，总算蹒跚地能走起来了，不过有一个很常用的功能 mixin，似乎还没有官方的解决方案。

既想享受 mixin 的灵活和方便，又想收获 ts 的类型系统带来的安全保障和开发时使用 IntelliSense 的顺滑体验。

vuejs 官方组织里有一个 'vue-class-component' 以及连带推荐的 'vue-property-decorator'，都没有相应实现。翻了下前者的 issue，有一条挂了好些时间的待做 feature 就是 mixin 的支持。

也不是什么复杂的事，自己写一个吧。

> 后注：vue-class-component 6.2.0 开始提供 [mixins][1] 方法，和本文的实现思路相似。

# 实现

```typescript
import Vue, { VueConstructor } from 'vue'

export type VClass<T> = {
  new(): T
} & Pick<VueConstructor, keyof VueConstructor>

/**
 * mixins for class style vue component
 */
function Mixins<A>(c: VClass<A>): VClass<A>
function Mixins<A, B>(c: VClass<A>, c1: VClass<B>): VClass<A&B>
function Mixins<A, B, C>(c: VClass<A>, c1: VClass<B>, c2: VClass<C>): VClass<A&B&C>
function Mixins<T>(c: VClass<T>, ...traits: Array<VClass<T>>): VClass<T> {
  return c.extend({
    mixins: traits
  })
}

```

声明 `VClass<T>` 可作为 T 的类构造器。同时通过 `Pick` 拿到 Vue 的构造器上的静态方法（extend/mixin 之类），如此才能够支持下面这段中的真正实现，通过调用一个 Vue 的子类构造器上的 `extend` 方法生成新的子类构造器。

``` typescript
function Mixins<T>(c: VClass<T>, ...traits: Array<VClass<T>>): VClass<T> {
  return c.extend({
    mixins: traits
  })
}
```

至于 ABC 这个纯粹是类型声明的体力活了。

# 使用

实际使用时:

```typescript
import { Component, Vue } from 'vue-property-decorator'
import { Mixins } from '../../util/mixins'

@Component
class PageMixin extends Vue {
  title = 'Test Page'

  redirectTo(path: string) {
    console.log('calling reidrectTo', path)
    this.$router.push({ path })
  }
}

interface IDisposable {
  dispose(...args: any[]): any
}

@Component
class DisposableMixin extends Vue {
  _disposables: IDisposable[]

  created() {
    console.log('disposable mixin created');
    this._disposables = []
  }

  beforeDestroy() {
    console.log('about to clear disposables')
    this._disposables.map((d) => {
      d.dispose()
    })
    delete this._disposables
  }

  registerDisposable(d: IDisposable) {
    this._disposables.push(d)
  }
}

@Component({
  template: `
  <div>
    <h1>{{ title }}</h1>
    <p>Counted: {{ counter }}</p>
  </div>
  `
})
export default class TimerPage extends Mixins(PageMixin, DisposableMixin) {
  counter = 0

  mounted() {
    const timer = setInterval(() => {
      if (this.counter++ >= 3) {
        return this.redirectTo('/otherpage')
      }
      console.log('count to', this.counter);
    }, 1000)

    this.registerDisposable({
      dispose() {
        clearInterval(timer)
      }
    })
  }
}

```

```
count to 1
count to 2
count to 3
calling reidrectTo /otherpage
about to clear disposables
```

注意到直接 `extends Vue` 的 `DisposableMixin` 并不是一个有效的 Vue 组件，也不可以直接在 `mixins` 选项里使用，如果要被以 `Vue.extend` 方式扩展的自定义组件使用，记住使用 `Component` 包装一层。

```typescript
const ExtendedComponent = Vue.extend({
  name: 'ExtendedComponent',
  mixins: [Component(DisposableMixin)],
})
```

# Abstract class

在业务系统中，多数情况需求下会更复杂，提供一些基础功能，但有些部分需要留给继承者自行实现，这个时候使用抽象类就很合适。

## 直接继承

```typescript
abstract class AbstractMusicPlayer extends Vue {
  abstract audioSrc: string
  
  playing = false
  
  togglePlay() {
    this.playing = !this.playing
  }
}

@Component
class MusicPlayerA extends AbstractMusicPlayer {
  audioSrc = '/audio-a.mp3'
}

@Component
class MusicPlayerB extends AbstractMusicPlayer {
  staticBase = '/statics'

  get audioSrc() {
    return `${this.staticBase}/audio-b.mp3`
  }
}
```

## 使用 Mixins

### 坏方法：欺骗，以及注释

但抽象类是无法被实例化的，并不满足 `{ new(): T }` 这个要求，因此只能被继承，而**不能**被混入，由于同样的原因，抽象类也无法被 'vue-class-component' 的 `Component` 函数装饰。

这时候只好将实现了的功能写入 Mixin 中，待实现的功能放到接口里，让具体类来实现。

```typescriptvue-class-component

interface IMusicSourceProvider {
  audioSrc: string
}

/**
 * 需要实现 IPlayerImplementation
 */
@Component
class PlayerMixin extends Vue {
  /** @abstract 必须实现 */
  audioSrc: string

  logSrc() {
    console.log(this.audioSrc)
  }
}

interface IPlayerImplementation extends IMusicSourceProvider {}

@Component
class OtherMixin extends Vue {
  description = '另一个 Mixin'
}

@Component
class RealPlayer extends Mixins(PlayerMixin, OtherMixin) implements IPlayerImplementation {
  audioSrc = '/audio-c.mp3'
}

// 无法正常工作
@Component
class BrokenPlayer extends Mixins(PlayerMixin, OtherMixin) {
}
```

鉴于 `@Component` 装饰器的实现方式，这种欺骗编译器的方式其实还是比较拙劣的。

如果一个具体类继承了 `PlayerMixin`，却没有使用 getter 或 property initializer 实现 `audioSrc` 这个属性，编译器无法告诉你这个错误（不开启严格模式的情况下），但实际使用中 `audioSrc` 其实是没有被初始化的，你会发现 `BrokenPlayer` 的实例当中 `_data` 里并不包含 `audioSrc`，即便在实例化后手动设置该值，Vue 也无法监听到该值的变化，会造成一些比较隐秘的 bug。

我们只能在代码里小心翼翼写上注释，期待使用者不要忘了这件事。

也可以执行一些开发时候的额外检查，如下：

#### 自定义装饰器 AbstractProperty

vue-class-component 提供了 [createDecorator][2] 方法来创建其体系下的自定义装饰器，我们可以这么用：

```typescript
import { createDecorator } from 'vue-class-component'

// 一个什么都不做的装饰器，在 production 环境下启用。不使用 createDecorator
const HolderDecorator = (ctor: any) => ctor

/**
 * Only for vue-class-component decorated class
 */
export const AbstractProperty = isProduction ? HolderDecorator:
createDecorator((options, key) => {
  const originCreated = options.created
  options.created = function () {
    if (originCreated) originCreated.apply(this, arguments)
    if (!(key in this)) {
      console.error(`未实现 AbstractProperty '${key}'`)
    }
  }
})


@Component
class PlayerMixin extends Vue {
  @AbstractProperty
  audioSrc: string

  logSrc() {
    console.log(this.audioSrc)
  }
}

@Component
class BrokenPlayer extends Mixins(PlayerMixin, OtherMixin) {
}
const player = new BrokenPlayer
// 未实现 AbstractProperty 'audioSrc'
```

### 没那么坏的方法：中间类

```typescript
@Component
class _PlayerImpl extends AbstractMusicPlayer {
  audioSrc = '/audio-d.mp3'
}

@Component
export class RealPlayer2 extends Mixins(_PlayerImpl, OtherMixin) {
}
```

使用中间类 `_PlayerImpl` 来实现抽象类的抽象部分，然后再被真正的使用者 `RealPlayer2` 使用。啰嗦了一点，但是类型安全。


[1]:	https://github.com/vuejs/vue-class-component/commit/cdcbfe5bb1a4c9d59a4b3066477f49c9aae8e2fc
[2]:	https://github.com/vuejs/vue-class-component#create-custom-decorators
