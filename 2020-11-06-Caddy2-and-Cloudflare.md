---
layout: post
date: 2020-11-06
title: Caddy 2 和 Cloudflare 配合
description: 让 Caddy Server 2 与 Cloudflare 能一起正常工作
use_toc: True
category: "折腾"
tags:
- Caddy
- 服务器
- 折腾
---

最近重装云主机，需要重新安装和部署以前一些个人站点，我的需求比较简单，一般就是部署静态文件或是 Vue/React SSR 应用，还有一些尝鲜的开源 self-host 项目([miniflux](https://github.com/miniflux/v2) / [plausible analytics](https://github.com/plausible/analytics) 等)。对于这样简单的静态文件服务加端口转发的需求来说，之前 Nginx + acme.sh 部署新站点的方式略微繁琐，决定使用 [Caddy](https://caddyserver.com/)。

Caddy 是一个用 Go 写的 server，一大主打特点是默认自动申请 HTTPS 证书和配置 http -> https 跳转，配置文件也很精简好读。

今年看见它已经稳稳地升到了 2，官网的文档也全都翻新了，配置方法与 1 有一些出入。

## 与 Cloudflare 结合遇上点小问题

我在有些子域名前有套上一层 Cloudflare 的 CDN，它代理我的网站后用的并不是我原来 caddy 自动申请的证书，而是 Cloudflare 自己签发一批证书。既然这样，如果我可以在服务器上也用 Cloudflare 签发的证书，就能解决问题。

## 安装和启用 Caddy

### 安装 xcaddy

需要安装插件 [caddy-dns/cloudflare: Caddy module: dns.providers.cloudflare](https://github.com/caddy-dns/cloudflare) 才能让 Cloudflare 与本机的通讯正常。

由于 caddy 是一个 go 的二进制包，如果需要添加一些非官方预打包的 plugin，就需要重新构建一次项目。官方提供的 xcaddy 就是这样一个自助编译的包。照着 [caddyserver/xcaddy: Build Caddy with plugins](https://github.com/caddyserver/xcaddy) READEME 指示操作，很快就成功了。

```
xcaddy build --with github.com/caddy-dns/cloudflare
```

### 解决问题 "listen tcp :443: bind: permission denied"

```bash
sudo setcap CAP_NET_BIND_SERVICE=+eip $(which caddy)
```

### Caddyfile 配置

插件需要使用 Cloudflare 的 API Token 才可以获取证书。操作方法见 https://dash.cloudflare.com/profile/api-tokens，将 token 填入下面的

```
http://wiki.hikerpig.cn {
  root * /home/myname/sites/wiki/_site
  encode gzip

  file_server browse

  log {
    output file /var/log/caddy/wiki-access.log
    roll_size 2MiB
  }
  tls {
    dns cloudflare {这里是我的 cloudflare api token}
  }
}
```

执行 `path/to/caddy start --config path/to/Caddyfile` 就能启动常驻的 caddy 程序了。

## 参考

- [Cloudflare 成功击毙了 Caddy server](https://melty.land/blog/caddy-and-cloudflare)
- [Caddy “listen tcp :443: bind: permission denied”](https://serverfault.com/questions/807883/caddy-listen-tcp-443-bind-permission-denied)

