---
layout: post
date: 2019-02-15
title: "Tips On Viewing Logs"
description: "一些看 LOG 时方便的工具和技巧"
category: Tips
tags:
- tip
- 工具
---

面对浩如烟海的 log，在排查错误和查找信息时使用一些趁手的工具，快速定位，关爱视力，有益身心健康。

# 高级的工具

## lnav

[lnav](http://lnav.org/) 是一个非常强大的日志查看器，有多种视图和过滤功能，不过最喜欢的其实还是自动的颜色高亮。

![lnav 官网给出的效果图](https://static1.squarespace.com/static/51bd4e13e4b0052d7873ad34/t/551aa43fe4b065952ea849ad/1427809346467/?format=1500w)

# 系统自带工具

不用安装额外的程序，linux 多数发行版都有的基础工具。

## less

### 高亮搜索命中内容

[Make Less highlight search patterns instead of italicizing them](https://unix.stackexchange.com/questions/179173/make-less-highlight-search-patterns-instead-of-italicizing-them)

```
export LESS_TERMCAP_se=$'\E[39;49m'
```
