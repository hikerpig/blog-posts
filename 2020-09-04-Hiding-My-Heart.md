---
layout: post
date: 2020-09-04
title: "Hiding My Heart 🎵"
category: 杂记
tags:
- 吉他
- 音乐
---

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="//music.163.com/outchain/player?type=2&id=16783834&auto=0&height=66"></iframe>

一首词曲创作和演唱都很优秀的乡谣，出自 Brandi Carlile 2007 年的专辑 _[The Story](https://music.douban.com/subject/2011571/)_ 。

![The Story](https://upload.wikimedia.org/wikipedia/en/0/01/Brandi_Carlile_-_The_Story.jpg)

词曲都由 Tim Hanseroth[^tim] (Timothy Jay Hanseroth) 所做，他是与 Brandi Carlile 常年合作的双胞胎兄弟之一。兄弟 Phil 是专辑同名曲 The Story 的作者，还和 Brandi 的妹妹结了婚 🤣。

C 大调的和弦和指法很简单，对初学者十分友好（甚至能把谱完整背下来）。没有用小调，没有复杂的编曲和节奏，主唱的音色和演唱都很直率和真诚，因此 "So I'll spend my whole life hiding my heart away" 这一句看着很矫情的歌词，听起来倒是也还好。

OneDrive 的嵌入组件还挺好用的，来一个在线版:

<iframe src="https://onedrive.live.com/embed?cid=5849D27F393060A3&resid=5849D27F393060A3%211962&authkey=AOxj110SpoYOMsk&em=2" width="476" height="800" frameborder="0" scrolling="no" seamless></iframe>

## 其他版本

2017 年的，该专辑十周年时，为给战争受灾儿童筹款，发布了纪念慈善专辑，里面收录了不同歌手翻唱的此专辑曲目。 [Adele 翻唱了这首歌](https://music.163.com/#/song?id=476553241)，节奏更为缓慢，凸显出阿呆醇厚丰富的嗓音。有趣的是这版的歌词主语对换了，原曲从歌词看来，是男生写给曾在家乡的恋人，而后女生去往大都市，因此二人分开了，而 Adele 版的歌词则是以女生作为主体，当然了，只有 You 和 I 的对换，并没有更改其他的歌词。

是单吉他伴奏，不过节奏型加入了一些花样，不会显得单调，但如果是自弹自唱用这个谱，记谱和练习可能要多花一点点时间。

## 吉他谱下载

- GuitarPro 版， [HIDING MY HEART INTERACTIVE TAB by Brandi Carlile for guitar and piano @ Ultimate-Guitar.Com](https://tabs.ultimate-guitar.com/tab/brandi-carlile/hiding-my-heart-guitar-pro-837015)
- TXT 版，[HIDING MY HEART INTERACTIVE TAB by Adele @ Ultimate-Guitar.Com](https://tabs.ultimate-guitar.com/tab/adele/hiding-my-heart-guitar-pro-1510524)
- GuitarPro 版，[HIDING MY HEART CHORDS (ver 2) by Adele @ Ultimate-Guitar.Com](https://tabs.ultimate-guitar.com/tab/adele/hiding-my-heart-chords-1053930)

[^tim]: [Songs written by Tim Hanseroth | SecondHandSongs](https://secondhandsongs.com/artist/90359)