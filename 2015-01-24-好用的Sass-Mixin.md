---
layout: post
date:   2015-01-24
title: "好用的Sass Mixin"
description: "好用的Sass Mixin"
category: 前端
tags:
- Sass

no_summary: True
use_toc: False
---

看下[CSS Trick上的帖子][Trick]


## 用:nth-child快速生成一个row

```css
@mixin rowMachine($numPerRow, $margin) {
  width: ((100% - (($numPerRow - 1) * $margin)) / $numPerRow);
  &:nth-child(n) {
    margin-bottom: $margin;
    margin-right: $margin;
  }
  &:nth-child(#{$numPerRow}n) {
    margin-right: 0;
    margin-bottom: 0;
  }
}
```

[Trick]: (http://css-tricks.com/video-screencasts/132-quick-useful-case-sass-math-mixins/)
