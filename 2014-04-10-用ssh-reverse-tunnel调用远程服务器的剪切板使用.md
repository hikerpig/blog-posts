---
layout: post
date:   2014-04-10
title: "用 ssh reverse tunnel 调用远程服务器的剪切板使用"
description: ""
category: Tips
tags:
- Vim
---

用ssh在远程服务器上工作的时候看着vim的满屏代码，突然怎么都没办法复制下来（其实鼠标还是可以用的，不过如果内容多到跨屏那就呵呵呵了），真是急死人，万能的google啊帮助我吧。

# 在本机上要做的事

先用netcat建立一个剪贴板服务在后台运行：

```bash
PORT=5566
while : ;do nc -l localhost $PORT | pbcopy ; done &
```

现在这两个命令的效果是一样的了：

```bash
echo "剪贴板" | pbcopy
echo "剪贴板" | nc localhost 5566
```

然后我们在ssh的时候加上反向代理参数:

```bash
# 表明当前ssh会话所有向远程服务器B的5566端口发送的信息都会转到本机A的5566端口上
ssh user@remote-server.com -R 5566:localhost:5566
```

现在在ssh会话中尝试下：

```bash
cat test_file.txt | nc -v localhost 5566
```

然后ctrl + v 试试，哈哈。

```bash
Connection to localhost 5566 port [tcp/*] succeeded!
```

去掉-v(verbose)选项可以让一切在默默中进行。

我整理出一个脚本，运行这个脚本就能搞出一个能偷用剪贴板的ssh会话了。

```bash
PORT=5566
ssh_server='ubuntu@crmdev'

# using mac
if [[ -n `which pbcopy` ]]; then
  echo "@Mac"
  copy_command='pbcopy'
elif [[ -n `which xsel` ]]; then
  echo "you can use xsel on ubuntu"
  copy_command='xsel'
else
  echo "you don't have clipboard utility"
fi

existed=`ps | grep nc | grep $PORT`

# start a netcat server
if [[ -n $existed ]]; then
  echo "nc $PORT already existed"
else
  echo "nc $PORT does not exist"
  if [[ -n $copy_command ]]; then
    while : ;do nc -l localhost $PORT | $copy_command ; done &
  fi
fi

# ssh the server
if [[ -n $copy_command ]]; then
  ssh -R $PORT:localhost:$PORT $ssh_server
else
  ssh $ssh_server
fi
```

## 其他方法

还有一个方法是[开启ssh的X11Forwarding选项](https://defuse.ca/blog/clipboard-over-ssh-with-vim)，还没试过。

# 在远程服务器需要做的事

使用vim编辑的时候，为了使用系统剪贴板并传到5566端口上，需要覆盖下我们在vim里的复制键位映射：

```vim
vmap "+y ::w !nc localhost 5566<CR>
```

其中`'<,'>`对应的是在Vim的Visual模式下选中的文本，使用`!nc`调用shell中的`nc`程序，便可以转发选中内容。

离鼠标依赖又远了一点。

# 参考文章
- [http://gistflow.com/posts/934-copy-from-a-remote-server-to-the-local-mac-clipboard](http://gistflow.com/posts/934-copy-from-a-remote-server-to-the-local-mac-clipboard)
- [exposing-your-clipboard-over-ssh][expose]
- [synchronize-pasteboard-between-remote-tmux-session-and-local-mac-os-pasteboard][sync]
- [vim-use-selected-text-as-input-to-a-shell-command][vselect]

[expose]: http://evolvingweb.ca/blog/exposing-your-clipboard-over-ssh
[sync]: http://superuser.com/questions/407888/synchronize-pasteboard-between-remote-tmux-session-and-local-mac-os-pasteboard
[vselect]: http://benninger.ca/posts/vim-use-selected-text-as-input-to-a-shell-command/
