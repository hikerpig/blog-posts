---
layout: post
date:   2016-12-21
title: "前端 Log 小记"
summary: "曾经有一个出现bug的页面出现在我面前，我没有珍惜，直到用户关掉页面了，才后悔莫及。"
category: 前端
tags:
- 前端
- Engineering
use_toc: True
feature_qimage: image/stack-or-gtfo.jpg
---

相信各位前端工程师们都经历过一个黑暗的场景。

{% qimage image/stack-or-gtfo.jpg %}

(哎哟哎哟测试大人我什么也没说)

曾经有一个出现bug的页面出现在我面前，我没有珍惜，直到用户关掉页面了，才后悔莫及。

而且错误不能等到用户反馈了才去探究，最好有完备的收集和响应系统，积极的错误日志分析，使我们防患于未然。


## 成熟的 SaaS 服务

选择还是挺多的，例如 [track.js][trackjs]，[Sentry][sentry]，[ErrorCeption](https://errorception.com/)，[Loggly](https://www.loggly.com/)，[Airbrke][airbrake]。都比较成熟，各家各有特色，集成相对简单。

### [{track.js}][trackjs]

鄙公司在使用的服务，十分好用，比起其他几个选项，只专注在前端错误日志，除了基本的记录、sourcemap支持以及错误栈追踪以外，提供的事件记录和时间轴功能，记录了所有网络请求以及用户操作(皆可配置开关)，能让你更好地重现异常发生的过程。十分推荐大家试试，试用期有30天，在自己的项目里先试试吧。


### [Sentry][sentry]

有跨语言和多框架集成支持，前后端皆可用，有免费的社区版本。

值得一说的是浏览器端的库名字是`raven`，看了不得不为这个梗点个赞。

![X-Men](http://www.30play.com/manage/editor/UploadFile/2014108143616129.jpg)

## 错误栈追踪(Stack Trace)

如果不使用自带 parser 的服务而是自己实现前后功能，可能在发送到服务器之前，最好做一些处理，例如生成更可读的错误栈，会让你工作更加轻松。栈信息对于定位和调试错误是十分重要的。

### 第三方库

#### [TraceKit](https://github.com/csnover/TraceKit)

```javascript
// 先注册一下报告的行为回调
TraceKit.report.subscribe(function yourLogger(errorReport) {
  // 发送ajax请求到服务器端
  // 在 https://gist.github.com/4491219 上有个好例子
};

// 开始写程序
try {
  /*
   * your application code here
   *
   */
  throw new Error('oops');
} catch (e) {
  TraceKit.report(e); //error with stack trace gets normalized and sent to subscriber
}
```


#### [stacktrace.js](http://stacktracejs.com/)

解析错误栈并且给出结构化表示，api 设计满足 Promise 规范。

官方给出的例子:

```javascript
var callback = function(stackframes) {
  var stringifiedStack = stackframes.map(function(sf) {
    return sf.toString();
  }).join('\n');
  console.log(stringifiedStack);
};

var errback = function(err) { console.log(err.message); };

window.onerror = function(msg, file, line, col, error) {
  // callback is called with an Array[StackFrame]
  StackTrace.fromError(error).then(callback).catch(errback);
};
```

### 提升开发时Console log体验的轻量第三方库

#### [Logdown](https://github.com/caiogondim/logdown.js)

node和浏览器端都可以使用的一个工具。

提供命名空间，并能分别开关各命名空间的log:

```javascript
var uiLogger = new Logdown({prefix: 'MyApp:UI'});
var networkServiceLogger = new Logdown({prefix: 'MyApp:Network'});

Logdown.disable('MyApp:UI');
Logdown.enable('MyApp:Network');
Logdown.disable('MyApp:*'); // 支持通配符
```

还支持 markdown 语法:

```javascript
var logger = new Logdown({markdown: true});
logger.warn('Log is *Awesome*');
```

可以看看作者给的[例子](https://caiogondim.github.io/logdown.js/)

{% qimage image/logdown.js.png %}

# 最后

错误日志收集完了记住要去看哟。

# 参考

- [Logging Errors in Client-Side Applications](https://www.sitepoint.com/logging-errors-client-side-apps/): 基本上是原文
- 题图来自[TraceKit](https://github.com/csnover/TraceKit)


[airbrake]: https://airbrake.io/
[trackjs]: http://trackjs.com/
[sentry]: https://sentry.io
