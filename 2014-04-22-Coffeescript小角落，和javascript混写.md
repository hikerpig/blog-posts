---
layout: post
date:   2014-04-22
title: "Coffeescript小角落，和javascript混写"
description: ""
category: 前端
use_toc: False
tags:
- Coffeescript

---

在[coffeescript-madness][coffee-madness]中提到了用coffee的`literal`来在函数内部强制生成局部变量，防止对外层变量的意外修改，例如coffee代码：

```coffeescript
y = 0
test = (x) ->
  `var y`
  y = 10
  x + y
```

会生成:

```js
var test, y;

y = 0;

test = function(x) {
  var y;
  y = 10;
  return x + y;
};
```

反引号内的内容会被直接打印到输出代码中，并且自动补全一个分号结尾。

在生产实践中是不会用这种歪门邪道的，就当知道了一个跟编译器玩捉迷藏的方法好了。



[coffee-madness]: https://donatstudios.com/CoffeeScript-Madness
