---
layout: post
date: 2020-07-02
title: "Toc Bar Userscript - 一个显示页面大纲的油猴脚本"
feature_image: https://raw.githubusercontent.com/hikerpig/toc-bar-userscript/master/images/screenshot-3.png
category: 玩具
tags:
- Userscript
- Toys
---

# Toc Bar

一个自用的油猴脚本，在页面中插入一个显示当前内容大纲的浮动组件，可[去 greasyfork 下载](https://greasyfork.org/zh-CN/scripts/406337-toc-bar)。

目前适配了一些个人经常访问的网站，主要面向技术阅读，知乎/Medium/devto/github 之类。

## ✨Features

- 使用 [tocbot](https://tscanlin.github.io/tocbot) 生成 Table of Content
- 对一些页面中 `h*` 标签不带 id 的网站，生成 id，以实现点击 TOC 标题跳转到对应内容的功能
- 不想使用的时候，组件可以一键折叠，避免挡住正在浏览的内容

## Screenshots

![devto](https://raw.githubusercontent.com/hikerpig/toc-bar-userscript/master/images/screenshot-2.png)

![tocbar-github.png](https://i.loli.net/2020/07/25/i3Ub8OwqrvaGzZX.png)

![zhuanlan-sspai](https://raw.githubusercontent.com/hikerpig/toc-bar-userscript/master/images/screenshot-1.jpg)


