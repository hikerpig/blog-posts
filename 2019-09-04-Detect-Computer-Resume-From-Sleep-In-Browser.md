---
layout: post
date: 2019-09-04
title: "在浏览器端检测电脑是否刚从休眠中醒来"
description: "当电脑进入休眠状态，浏览器的一些持久功能会受到影响(例如 WebSocket 连接中断)，有时需要在从休眠状态中恢复过来的时候做一些检查和恢复工作"
category: 前端 
no_summary: True
tags:
- 浏览器
---
    
当电脑进入休眠状态，浏览器的一些持久功能会受到影响(例如 WebSocket 连接中断)，有时需要在从休眠状态中恢复过来的时候做一些检查和恢复工作。

# 解决方案 

## 利用计时器   

```js
let lastTime = (new Date()).getTime()

// 可能由于脚本计算量大，或是使用了 alert/confirm 等阻塞线程的函数，的确会有些小误差，可以忽略掉
const THRESHOLD = 15000

setInterval(function() {
  const currentTime = Date.now()
  if (currentTime > (lastTime + THRESHOLD)) {
    // 刚醒来，做点什么呢
  }
  lastTime = currentTime
}, 30000); // 30s 比较保守，可以根据业务需求来调整
```

# 参考

- [Can any desktop browsers detect when the computer resumes from sleep?](https://stackoverflow.com/questions/4079115/can-any-desktop-browsers-detect-when-the-computer-resumes-from-sleep)