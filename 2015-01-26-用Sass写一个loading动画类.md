---
layout: post
date:   2015-01-26
title: "用Sass写一个loading动画类"
description: ""
category: 
tags: []

use_toc: False
---

继续 Codepen 上的Sass魔法门。

[这位童鞋][Zero]做了两个loading动画的样式

```scss
@keyframes loading {
  0% { transform: translate(-99px, -50%); }
  100% { transform: translate(71px, -50%); }
}
```

```scss
//This is not the best example of a mixin - it is just something to help modify examples on the fly. Sorry! 

@mixin loading($name, $big_radius:50,$little_radius:10,$big_stroke:10,$little_stroke:5,$speed:4s,$res:40) {
  &:before{
    content:'';
    position: absolute;
    top: 50%;
    left: 50%;
    width: ($big_radius*2) +px;
    height: ($big_radius*2) +px;
    z-index: 999;
    border-radius: 50%;
    border: solid $big_stroke+px rgba(10,120,255,.5);
    transform: translate( 0- $big_radius + px, 0- $big_radius +px);
  }
  &:after{
    content:'';
    position: absolute;
    top: 50%;
    left: 50%;
    width: $little_radius+px;
    height: $little_radius+px;
    z-index: 999;
    border-radius: 50%;
    border: solid $little_stroke+px rgba(255,255,255,.8);
    animation-name: #{$name} ;
    animation-duration: $speed;
    animation-iteration-count: infinite;
  }
  // This generates a lot of keyframes, but smaler than svg or img, can tune with $res
  @keyframes #{$name} {
    @for $i from 0 through $res {
      $x: floor( ($big_radius - ($big_stroke/2)) * cos( 2 * pi() * ($i / $res)) - ($little_radius/2) ); 
      $y: floor( ($big_radius - ($big_stroke/2)) * sin( 2 * pi() * ($i / $res)) - ($little_radius/2) );
      #{($i/$res)*100}% { transform: translate($x + px,$y + px); }
    }
  } 
}


```

```scss
.example-1{
  height: 200px;
  margin: 1em 0;
  background: rgba(100,230,230,0.5);
  border: solid 3px rgb(100,230,230);
  position: relative;
  @include loading('example-1',80,20,20,10);
}

.example-2{
  margin: 1em 0;
  padding: 0 1em;
  border: solid 3px rgb(100,230,230);
}

#more{
  position: relative;
  display: block;
  margin: 1em 0;
  padding: 1em;
  height: 65px;
  text-align: center;
  border: dashed 3px rgb(100,230,230);
}

#more:target{
  position: relative;
  background: rgba(100,230,230,0.5);
  border: solid 3px rgb(100,230,230);
  @include loading('example-2',20,10,5,5,1s);
}

#more:target a{
  display: none;
}

.example-3{
  position: relative;
  margin: 1em 0;
  cursor:pointer;
}
.example-3:hover{
  @include loading('example-3',30,20,8,5,2s);
}

.example-4{
  position: relative;
  display: block;
  margin: 1em 0;
  padding: 1em;
  height: 65px;
  text-align: center;
  border: dashed 3px rgb(100,230,230);
}

#page:target{
  position:absolute;
  position:fixed;
  top:0;
  left:0;
  width:100%;
  height:100%;
  z-index: 99999;
  background:rgba(255,255,255,0.9);
  @include loading('example-4',60,20,15,8,3s);
}
```

[Zero]: http://madebymike.com.au/writing/zero-element-loading-animations/
