---
layout: post
date: 2020-10-14
title: "使用 zimfw 作为 zsh 配置框架"
description: "使用 zimfw 替换 oh-my-zsh，提升 zsh 启动速度"
category: 工具
use_toc: true
tags:
- zsh
- 折腾
---

之前使用 oh-my-zsh 的自带 git/nvm 等插件有时候有性能问题，同时据说其框架本身相比其他一些主打速度的配置框架(prezto/zimfw 等)也会慢一点。

稍微折腾了一下，使用 [zimfw](https://github.com/zimfw/zimfw) 作为 zsh 配置框架，它在[速度](https://github.com/zimfw/zimfw/wiki/Speed)和可配置性间达到了很好的平衡。

个人切换后，试了下跑 zprof，启动时间只有原先使用 oh-my-zsh 时的一半，且没有明显感觉缺失常用功能，度量方式参考 [Profiling zsh startup time][profiling-zsh-startup]。

## 安装

按照[项目 README 中的安装指南跑一遍就好](https://github.com/zimfw/zimfw#installation)，不超过一分钟。

## 插件加载

用上[自带的模块](https://github.com/zimfw/zimfw/wiki/Modules)和[zsh-users](https://github.com/zsh-users/)里一些优秀的插件，就已经有很好的表现，其中 zsh-users/zsh-autosuggestions 和 zsh-users/zsh-syntax-highlighting 能让 zsh 也有类似于 fish 一样友好的代码高亮反馈和输入自动补全功能。zimfw 比较友好的一点就在于：默认配置（使用其提供的安装脚本）里给出的组合就已经很棒了。

添加插件的方式与 oh-my-zsh 不太一样，后者需要把插件添加到指定的 plugins 目录（一般为 clone repo 或者拷贝文件），然后在 `.zshrc` 文件里 `plugins=()` 数组里添加插件名。

而 zimfw 的方式则稍微方便一点，除了类似后者的方式外，还可以不用手动操作目录而是在 `.zimrc` 文件中添加模块名（一般为 github 上的 user/repo 名），可在 [awesome-zsh-plugins](https://github.com/unixorn/awesome-zsh-plugins) 里寻找合适插件，多数与配置框架无关(可以不局限在 oh-my-zsh 提供的插件目录里)。

修改配置文件后，使用命令 `zimfw install` 下载并编译插件(会使用 zsh builtin 的 `zcompile` 将 `.zsh` 文件编译为 `.zwc` 字节码，提高下次加载的速度)，打开新 shell 时新插件生效。

升级插件也很简单，`zimfw update` 即可，安装完成会列出升级了插件新增的 git commits。

## 外观和主题

zimfw 的主题远不如 oh-my-zsh 丰富，自己的 gallery 里都是偏向极简的风格，不过我们可以自己找一些成熟的主题 [powerlevel10k](https://github.com/romkatv/powerlevel10k) 使用，带有问答式的新手配置指导，功能丰富，推荐使用。
主题默认的效果，可以在 prompt 里显示一些有用的附加信息。
![](https://raw.githubusercontent.com/romkatv/powerlevel10k-media/master/prompt-highlight.png)
powerlevel10k 的 gitstatus 功能效果很好，据说比 oh-my-zsh 里的 git 插件快，没有评测过。

## 一些配置

`~/.zimrc`

```zsh
# Start configuration added by Zim install {{{
# -------
# Modules
# -------
# Sets sane Zsh built-in environment options.
zmodule environment
# Provides handy git aliases and functions.
# zmodule git
# Applies correct bindkeys for input events.
zmodule input
# Sets a custom terminal title.
zmodule termtitle
# Utility aliases and functions. Adds colour to ls, grep and less.
zmodule utility

# Prompt
#
# Exposes git repository status information to prompts.
zmodule git-info
# zprompt_theme='powerlevel10k'

# Theme
# zmodule sorin
# zmodule eriner
zmodule romkatv/powerlevel10k

# Additional completion definitions for Zsh.
zmodule zsh-users/zsh-completions
# Enables and configures smart and extensive tab completion.
# completion must be sourced after zsh-users/zsh-completions
zmodule completion
# Fish-like autosuggestions for Zsh.
zmodule zsh-users/zsh-autosuggestions
# Fish-like syntax highlighting for Zsh.
# zsh-users/zsh-syntax-highlighting must be sourced after completion
zmodule zsh-users/zsh-synta-highligxhting
# Fish-like history search (up arrow) for Zsh.
# zsh-users/zsh-history-substring-search must be sourced after zsh-users/zsh-syntax-highlighting
zmodule zsh-users/zsh-history-substring-search
# }}} End configuration added by Zim install


# Custom 个人推荐的插件
zmodule directory
zmodule Aloxaf/fzf-tab # 将 zsh shell 补全的结果 pipe 入 fzf 中显示和选择

# 个人其他工具的选择
zmodule wookayin/fzf-fasd
zmodule zsh-users/zaw

zmodule greymd/docker-zsh-completion
zmodule DarrinTisdale/zsh-aliases-exa # 添加多个 alias, 使用 exa 代替 ls，要求有安装 exa
```

介绍一些程序和插件:

- [快速跳转工具--FASD 简单介绍](https://segmentfault.com/a/1190000011327993)

## 参考文章

- [打造屬於你自己的極速 Shell「iTerm + zsh + zim + powerlevel10k」](https://www.jkg.tw/p2876/)
- [打造 10X Engineer Shell 工作環境. SRE 討生活篇 | by smalltown | Starbugs Weekly 星巴哥技術專欄 | Medium](https://medium.com/starbugs/%E6%89%93%E9%80%A0-10x-engineer-zsh-shell-97e40db76391)
- [Faster and enjoyable ZSH](https://htr3n.github.io/2018/07/faster-zsh)
- [Quit using nvm：快删掉这个占据 Zsh 启动时间一半的怪物！ - Spencer's Blog](https://blog.spencerwoo.com/2020/07/remove-nvm-to-speed-up-zsh/)

[profiling-zsh-startup]: https://stevenvanbael.com/profiling-zsh-startup

