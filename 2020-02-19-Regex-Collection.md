---
layout: post
date: 2020-02-19
title: "我的常用正则表达式清单"
description: ""
category: Engineering
tags:
- 工具
- 正则
use_toc: True
---

## 转换

[medium 的一篇文章有列出几种常见命名方式转换的 JS 函数](https://medium.com/javascript-in-plain-english/convert-string-to-different-case-styles-snake-kebab-camel-and-pascal-case-in-javascript-da724b7220d7)

### CamelCase -> kebab-case

javascript 使用示例

```js
string.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase()
```

其中使用了正向前瞻 (positive lookahead)，有一些 [参考文章](https://www.cnblogs.com/boundless-sky/p/7597631.html) 可以学习一下。

### kebab-case/snake_case -> CamelCase

```js
const toCamelCase = str =>
  str
    .toLowerCase()
    .replace(/[^a-zA-Z0-9]+(.)/g, (m, char) => char.toUpperCase())
```
