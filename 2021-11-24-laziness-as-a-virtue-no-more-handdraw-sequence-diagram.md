---
layout: post
date: 2021-11-24
title: 懒惰是美德 ✌ 别再用手拖时序图了
description: ""
summary: "文本 DSL 生成各种常见 UML 示意图"
published: True
category: 工具
use_toc: True
tags:
- 工具
- PlantUML
---

## Diagram as Text

可以省去手绘 UML 图表时调整布局等的时间。
通过文本 DSL 生成图表。文本本身也具有一些可读性，以及（划重点！）对 git diff 友好。在不断演变的技术文档内使用还是比较合理的。

## 适合什么情况
- 画常用的几种 UML 图表（时序图、类图、流程图、部署图等）。
- 处于方案的早期设计和交流阶段，快速修改。

## 不适合什么情况
这样的情况建议还是手绘
- 难以归入标准 UML 的示意图
- 复杂的架构图

# 推荐工具

## PlantUML（推荐）

[开源工具，使用简单的文字描述画UML图。](https://plantuml.com/zh/)

PlantUML 的功能应该是在它这个领域（text to UML diagram）里最多最全的。完整功能请看文档。

![plantuml-demo](https://aws1.discourse-cdn.com/standard14/uploads/cozic/original/2X/f/f25350393d74576508dff7d85cb2fb53da519f28.png)

### 在线编辑器

- [PlantUML Editor](https://plantuml-editor.kkeisuke.com/)，在线的 PlantUML 编辑器，体验比官网的编辑器界面稍微好一点（主要是好看）。

### VSCode 插件

在 VSCode 插件市场里搜索 PlantUML 还是有不少的，找了个下载量最大的 https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml 。

支持高亮 *.puml 等文件，也能在本地实时预览（可能需要安装 PlantUML 的 jar 包）。

### 优缺点

优点:
- 功能丰富
- 具有可扩展性（元素分组、宏、样式等），可以 import 代码片段来扩展功能，有 C4-PlantUML 这样的高阶包。
- 本地运行速度很快

缺点:
- 使用 Java 写的，在线实时预览有点麻烦，没法在浏览器中离线完成。
- 基本找不到好看的主题 = =，当然如果习惯了默认主题的返璞归真，此条就没有任何问题。

## Mermaid.js

大量地借鉴了 PlantUML 语法，有所重叠，但不完全一样。算是 PlantUML 的弱化版，但也有一些其他长处。

看看文档和输出的图表，是不是比 PlantUML 好看多了。

![](https://i.loli.net/2021/11/24/ALQWMr3ywFalVIZ.png)

用 JS 写的，赶上了 web 技术大潮，有许多使用 web 技术的 Markdown 编辑器应用（如 Notable ）自带对它的支持。

### 在线编辑器

看这个官方的 [live-editor](https://mermaid-js.github.io/mermaid-live-editor/edit/#) ，尽显丝滑。

### VSCode 插件
在 VSCode 插件市场里搜索 mermaid 也有很多，而且在 markdown 文件里加入 mermaid 类型的 code block ，也能有代码高亮和本地预览。

支持高亮 `*.mermaid` 等文件，也能在本地实时预览。

装好了 Markdown 支持的插件后，还可以在 Markdown 文件的 mermaid 代码块中看见语法高亮，写技术博客很舒服。

### 优缺点

优点:
- 它是用 JS 写的，在网页端做实时预览非常容易，不需要走服务端请求生成图
- 具有一部分可扩展性，但没有 PlantUML 丰富

缺点:
- 功能（相对于 PlantUML)稍弱，但是在 JS 的世界里还是可以的
- 一开始架构设计没有考虑太多，只要在浏览器端使用就行。直接使用 d3 和非常依赖浏览器的布局计算。如果采用实时生成的方式，只有 SVG 输出。在 node 端输出 png 图片的话，还需要下一个无头浏览器，略惊悚。

## Pintora

【广告预警！】我自己的开源项目 [Pintora](https://pintorajs.vercel.app/)，还在 alpha 阶段。使用 TypeScript 写成，对浏览器和 Node.js 都有好，比起 Mermaid.js 来说有更合理的架构分层和对开发者更友好的扩展性。

- 在浏览器端，支持 SVG 和 Canvas 输出
- 在 Node.js 端，支持输出 PNG/JPG 位图
- 具有高度的可组合性，按需加载图表类型，核心代码可控制在较轻量级
- 具有高度的可扩展性，开发者可扩展自己的图表，作为插件接入，详情请见[实现你自己的图表](https://pintorajs.vercel.app/zh-CN/docs/advanced/write-a-custom-diagram/)

目前实现了几种常见 UML 图表：

- [时序图 Sequence Diagram](https://pintorajs.vercel.app/zh-CN/docs/diagrams/sequence-diagram/)
- [Entity Relationship Diagram](https://pintorajs.vercel.app/zh-CN/docs/diagrams/er-diagram/)
- [组件图 Component Diagram](https://pintorajs.vercel.app/zh-CN/docs/diagrams/component-diagram/)

<div class="post__image-row">
  <img src="https://i.loli.net/2021/11/24/1oELgS8ibMHRa7U.png" height="600">
  <img src="https://i.loli.net/2021/11/24/vV1Z5Ir3Nz2oMgT.png" height="600">
</div>

## Kroki.io

https://kroki.io/ 严格来说并不是一个单独的库，而是很多种常用 text-to-diagram 工具的 Web API 调用集合，部署一套服务就可以使用 url 作为标识来预览十几种工具的结果。
顺藤摸瓜可以看到一些优秀的工具。
